﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnboundPatrol : MonoBehaviour {

    public int EnemySpeed = 1;
    public int XMoveDirection = 1;
    public float RangeFinder = 1.0f;
    //public SpriteRenderer rend;


    void start()
    {
        //rend = this.GetComponent<SpriteRenderer>();
        //print(rend);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, new Vector2(XMoveDirection, 0));
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(XMoveDirection, 0) * EnemySpeed;


        for (int i = 0; i < hits.Length; i++)
        {
            //print()

            //if (hits[i].collider.name != this.name || hits[i].collider.tag == "Player")
            if (hits[i].collider.tag == "Wall")
            {
                if (hits[i].distance < RangeFinder)
                {

                    XMoveDirection = -XMoveDirection;

                    if (XMoveDirection < 0)
                    {
                        gameObject.GetComponent<SpriteRenderer>().flipX = true;
                    }
                    else
                    {
                        gameObject.GetComponent<SpriteRenderer>().flipX = false;
                    }
                }
            }
        }
    }
}
