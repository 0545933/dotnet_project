﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProjectile : MonoBehaviour {

    public float Interval = 2;
    public bool Vertical = true;
    public float Lifespan = 1;
    public GameObject projectile;


    void Start()
    {
        InvokeRepeating("LaunchProjectile", Interval, Interval);
    }


    void LaunchProjectile()
    {
        GameObject clone;
        clone = Instantiate(projectile, transform.position, transform.rotation);
        clone.tag = "InflictsDamage";
        clone.GetComponent<InflictsDamage>().lethal = false;
        clone.GetComponent<InflictsDamage>().armed = true;
        clone.GetComponent<InflictsDamage>().damage = 1;
        clone.GetComponent<InflictsDamage>().destroyOnContact = true;
        clone.GetComponent<Rigidbody2D>().velocity = clone.transform.forward * 6;


        if (Vertical == true)
        {
            clone.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
        }

        else
        {
            clone.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;

            //clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * 6;

            if (this.GetComponentInParent<SpriteRenderer>().flipX == true)
            {
                clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * 6;
            }
            else
            {
                clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * -6;
            }
            
        }
        


        

        Destroy(clone, Lifespan);

    }
}

