﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionManager : MovingObject
{
    public PlayerMovement player;
    //public Health hitpoints;


    void OnTriggerEnter2D(Collider2D col)
    {
        player = gameObject.GetComponent<PlayerMovement>();

        switch (col.tag)
        {
            case ("InflictsDamage"):
            {
                if (col.GetComponent<InflictsDamage>().armed == true)
                {
                    print("***Encountered object that inflicted damage***");
                    SoundManager.instance.PlaySingle(player.exitSound);

                        if (col.GetComponent<InflictsDamage>().lethal == true)
                        {
                            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                        }
                        else
                        {
                            player.currentHealth--;
                        }
                }
                break;
            }
            case ("Finish"):
            {
                print("***Encountered Exit***");
                //Plays the audio for exiting the level
                SoundManager.instance.PlaySingle(player.exitSound);
                switch (SceneManager.GetActiveScene().name)
                {
                    case ("Level1"):
                    {
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                        break;
                    }
                    case ("Level2"):
                    {
                        SceneManager.LoadScene("MainMenu");
                        break;
                    }
                    default:
                    {
                        SceneManager.LoadScene("MainMenu");
                        break;
                    }

                }
                break;
            }
            case ("Collectible"):
            {
                try
                {
                    print("***Encountered Collectible***");
                    //Plays collect audio clip
                    SoundManager.instance.PlaySingle(player.collectSound);
                    //Disables the collectible object
                    col.gameObject.SetActive(false);
                    //Increments player points
                    player.points++;
                    print("Total Points:  " + player.points.ToString());
                }
                catch (Exception ex)
                {
                    print(ex);
                }
                break;
            }
            case ("Sign"):
            {
                print("Encounteres Sign");
                try
                {

                    Canvas sign;

                    sign = col.GetComponentInChildren<Canvas>();
                    print("Disabling sign:" + sign);
                    sign.enabled = true;
                    print("Sign Disabled");

                    
                }
                catch (Exception ex)
                {
                    print(ex);
                }
                break;
            }
        }
    }



    void OnTriggerExit2D(Collider2D col)
    {
        player = gameObject.GetComponent<PlayerMovement>();

        switch (col.tag)
        {
            case ("Sign"):
            {
                Canvas sign;

                sign = col.GetComponentInChildren<Canvas>();
                sign.enabled = false;
                break;
            }

        }
    }
}
 