﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour {

    public float Delay;
    private float DelayUnit;
    public bool Deployed;
    Animator animator;



    void Start()
    {
        DelayUnit = Delay;
        animator = GetComponent<Animator>();
        animator.speed = 0f;
    }



    void Update()
    {
        DelayUnit -= Time.deltaTime;

        if (DelayUnit <= 0)
        {
            if (Deployed == true)
            {
                animator.speed = 0.5f;
                Deployed = false;
            }
            else
            {
                //animator. = 1.0f;
                animator.speed = -0.5f;
                Deployed = true;
            }

            DelayUnit = Delay;
        }
    }





}
