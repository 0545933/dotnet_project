﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MovingObject
{
    //Setup variables
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    public int points;  //points from collectibles
    public AudioClip walkSound;
    public AudioClip runSound;
    public AudioClip jumpSound;
    public AudioClip collectSound;
    public AudioClip exitSound;

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        points = 0;
    }

    //OnTriggerEnter2D is sent when another object enters a trigger collider attached to this object (2D physics only).
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check if the tag of the trigger collided with is Exit.
        if (other.tag == "Finish")
        {
            //Plays the audio for exiting the level
            SoundManager.instance.PlaySingle(exitSound);
            //Loads the Scene by its name or index in Build Settings.
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        //Check if the tag of the trigger collided with is the Collectible
        if (other.CompareTag("Collectible"))
        {
            //Plays collect audio clip
            SoundManager.instance.PlaySingle(collectSound);
            //Disables the collectible object
            //other.gameObject.SetActive(false);
            //Increments player points
            //points++;
            Debug.Log("Total Points:  " + points.ToString());
        }
    }

    //Calculate Jumping and Horizonal Movement
    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;                    //Create 2D vector
        move.x = Input.GetAxis("Horizontal");           //Capture user input (A,D || <--, -->)

        if (Input.GetButtonDown("Jump") && grounded)    //Test for jump conditions
        {
            velocity.y = jumpTakeOffSpeed;              //Add Vertical velocity to player
        }
        else if (Input.GetButtonUp("Jump"))             //Test if jump button is released
        {
            if (velocity.y > 0)                         //Test if vertical velocity is positive
            {
                velocity.y = velocity.y * 0.5f;         //Dampen Vertical velocity by half
            }
        }

        //If player is stationary on the ground
        if (move.x == 0.0f && grounded)
        {
            animator.SetBool("playerIdle", true);
            //Debug.Log("idle");
        }

        //If player is moving on the the ground
        if (move.x != 0.0f && grounded)
        {
            SoundManager.instance.PlaySingle(runSound);
            animator.SetBool("playerRun", true);
            // Debug.Log("running");
        }

        if (velocity.y > 0.5f)
        {
            //SoundManager.instance.PlaySingle(jumpSound);
            animator.SetBool("playerJump", true);
            //Debug.Log("jumping");
        }

        if (velocity.y < -0.2f)
        {
            animator.SetBool("playerFall", true);
            //Debug.Log("falling");
        }

        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.LeftControl))
        {
            animator.SetBool("playerCrouch", true);
            Debug.Log("crouch");
        }

        if (move.x > 0.01f)
        {
            if (spriteRenderer.flipX == true)
            {
                spriteRenderer.flipX = false;
            }
        }

        else if (move.x < -0.01f)
        {
            if (spriteRenderer.flipX == false)
            {
                spriteRenderer.flipX = true;
            }
        }

        animator.SetBool("grounded", grounded);

        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }

}