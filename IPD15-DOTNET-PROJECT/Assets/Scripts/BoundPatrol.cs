﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundPatrol : MonoBehaviour {

    public int EnemySpeed = 1;
        private int XMoveDirection = 0;
    public float WanderRange = 5;
        //private float PointOne;
        //private float PointTwo;
        private float TargetPoint;
        private float Origin;
        private float distance;


    // Update is called once per frame
    void Update()
    {
        if (XMoveDirection == 0)
        {
            Origin = transform.position.x;
            XMoveDirection = -1;
            //PointOne = transform.position.x + WanderRange;
            //PointTwo = transform.position.x - WanderRange;
            TargetPoint = Origin + WanderRange;
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(XMoveDirection, 0) * EnemySpeed;

        if (XMoveDirection == 1)
        {
            distance = (Origin + WanderRange) - transform.position.x;
        }
        else
        {
            distance = transform.position.x - (Origin - WanderRange);
        }


        if (distance < 1.0f)
        {
            if (XMoveDirection == 1)
            { 
                TargetPoint = (Origin - WanderRange);
            }
            else if (XMoveDirection == -1)
            {
                TargetPoint = (Origin + WanderRange);
            }

            XMoveDirection = -XMoveDirection;



            if (XMoveDirection < 0)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }  
    }
}