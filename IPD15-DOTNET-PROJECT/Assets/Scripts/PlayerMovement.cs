using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {

    //Refrences
	public CharacterController2D controller;
    public Animator animator;

    //Settings
	public float runSpeed = 40f;

    //State Controls
	float horizontalMove = 0f;
	bool jump = false;
	bool crouch = false;
    bool hurt = false;
    
    //Stats
    public int points;
    public int currentHealth = 0;
    public int maxHealth = 6;
    
    //Sound
    public AudioClip walkSound;
    public AudioClip jumpSound;
    public AudioClip collectSound;
    public AudioClip exitSound;


    //Setup Player
    void Start()
    {
        currentHealth = maxHealth;
        points = 0;
    }


    // Update is called once per frame
    void Update ()
    {
        if (currentHealth <= 0)
        {
            GameOver();
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

		if (Input.GetButtonDown("Jump"))
		{
			jump = true;
            SoundManager.instance.PlaySingle(jumpSound);
            animator.SetBool("IsJumping", true);
            print("Jumped");
		}

		if (Input.GetButtonDown("Crouch"))
		{
			crouch = true;
		} else if (Input.GetButtonUp("Crouch"))
		{
			crouch = false;
		}
	}



    public void Damage(int dmg)
    {
        currentHealth -= dmg;
    }

    void GameOver()
    {
        //Loads the Scene by its name or index in Build Settings.
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }




    void FixedUpdate()
    {
        // Move Player
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }





    public void OnLanding()
    {   animator.SetBool("IsJumping", false);   }


    public void OnCrouch(bool isCrouching)
    {   animator.SetBool("IsCrouching", isCrouching);   }


    






}
