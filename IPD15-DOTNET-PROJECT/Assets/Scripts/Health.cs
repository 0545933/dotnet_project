﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Sprite[] HeartSprites;
    public Image HeartUI;
    //access the health of player
    private PlayerMovement player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            print("Updating Health");
            print("HearyUI size: " + HeartSprites.Length);
                                                HeartUI.sprite = HeartSprites[player.currentHealth - 1];
            print("Health: " + player.currentHealth);
        }
        catch (Exception ex)
        {
            print(ex);
        }
    }
}
