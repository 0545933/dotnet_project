﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public PlayerMovement player;
    private TextMeshProUGUI scoreText;

    private void Start()
    {
        // Get a reference to an existing TextMeshPro component
        scoreText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        // Set the text
        scoreText.text = string.Format("Score: {0}", player.points);
    }
}
