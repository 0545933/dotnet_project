﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timed_Toggle : MonoBehaviour
{
    public float Delay;
    private float DelayUnit;
    public bool Deployed;
    private Animator animator;
    private InflictsDamage inflictor;

    void Start()
    {
        DelayUnit = Delay;
        animator = GetComponent<Animator>();
        inflictor = GetComponent<InflictsDamage>();

        if (Deployed == true)
        {
            animator.SetBool("Deploy", true);
        }
    }



    void Update()
    {
        DelayUnit -= Time.deltaTime;

        if (DelayUnit <= 0)
        {
            
            if (Deployed)
            {
                animator.SetBool("Retract", true);
                Deployed = false;
                inflictor.armed = true;
            }
            else
            {
                animator.SetBool("Deploy", true);
                Deployed = true;
                inflictor.armed = false;
            }

            DelayUnit = Delay;
        }
    }
}