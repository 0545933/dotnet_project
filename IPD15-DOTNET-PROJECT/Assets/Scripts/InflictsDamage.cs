﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InflictsDamage : MonoBehaviour
{
    public bool lethal = false;
    public bool armed;
    public int damage = 0;
    public bool destroyOnContact;

    void OnTriggerEnter2D(Collider2D other)
    {
        print("other tag " + other.tag);
        print("destroy? " + destroyOnContact);

        if ((other.tag == "Player" || other.tag == "Wall") && destroyOnContact == true)
        {
            Object.Destroy(gameObject);
        }
    }
}


