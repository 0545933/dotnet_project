﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchControl : MonoBehaviour {

    [SerializeField] private Sprite switchedOff;
    [SerializeField] private Sprite switchedOn;
    [SerializeField] private GameObject Controls;
    public bool status = false;
    private SpriteRenderer renderer;


    // Use this for initialization
    void Start ()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        CheckStatus();
    }























    // Update is called once per frame
    void OnCollisionStay (Collision2D c)
    {
        print("triggered");
        if (Input.GetKeyDown(KeyCode.E))
        {
            print("key");
            status = !status;
            //Controls.Toggle();
            CheckStatus();
        }
         
    }

    private void CheckStatus()
    {
        if (status == true)
        {
            renderer.sprite = switchedOn;
        }
        else
        {
            renderer.sprite = switchedOff;
        }


    }
}
